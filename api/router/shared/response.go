package shared

import (
	"encoding/json"
	"net/http"
)

var (
	NotYetImplementedError = "This endpoint is not yet implemented!"
)

type Response struct {
	Success bool        `json:"success"`
	Reason  string      `json:"reason,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func Error(w http.ResponseWriter, message string, data ...interface{}) {
	res := Response{
		Success: false,
		Reason:  message,
	}
	if len(data) != 0 {
		res.Data = data
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)
}

func Success(w http.ResponseWriter, data interface{}) {
	res := Response{
		Success: true,
		Data:    data,
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)
}
