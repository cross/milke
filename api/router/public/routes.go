package public

import (
	"milke/router/public/analytics"
	"net/http"
)

type Route struct {
	Name    string
	Method  string
	Pattern string
	Handler http.HandlerFunc
}

var routes = []Route{
	{
		"Analytics.Get",
		"GET",
		"/analytics",
		analytics.Get,
	},
}
