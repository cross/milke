package private

import (
	"milke/router/private/user"
	"net/http"
)

type Route struct {
	Name    string
	Method  string
	Pattern string
	Handler http.HandlerFunc
}

var routes = []Route{
	{
		"User.Create",
		"POST",
		"/user/new",
		user.New,
	},
}
