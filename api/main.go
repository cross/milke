package main

import (
	"log"
	"milke/database"
	"milke/router/private"
	"milke/router/public"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type Application struct {
	Logger   *log.Logger
	Database *gorm.DB
	Layers   *Layers
}

type Layers struct {
	private, public *mux.Router
}

func main() {
	app := initialize()
	app.start()
}

// Should get this function checked out.
func (layers *Layers) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	url := strings.Split(r.Host, ".")

	if len(url) == 3 {
		if url[0] == "api" {
			layers.private.ServeHTTP(w, r)
		} else {
			layers.public.ServeHTTP(w, r)
		}
	} else {
		return
	}
}

func initialize() Application {
	app := Application{}

	app.Logger = log.New(os.Stdout, "[>] ", log.Lshortfile)

	app.Layers = &Layers{
		private: private.CreateRouter(),
		public:  public.CreateRouter(),
	}

	if db, err := database.Connect(); err != nil {
		if err != nil {
			app.Logger.Fatal(err)
		} else {
			app.Database = db
		}
	}

	return app
}

func (app Application) start() {
	app.Logger.Fatal(http.ListenAndServe(":80", app.Layers))
}
