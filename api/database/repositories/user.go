package repositories

import (
	"milke/database/models"

	"gorm.io/gorm"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(database *gorm.DB) *UserRepository {
	return &UserRepository{
		db: database,
	}
}

func (u *UserRepository) Create(user models.User) (int64, error) {
	result := u.db.Create(&user)
	return result.RowsAffected, result.Error
}
