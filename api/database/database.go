package database

import (
	"milke/database/models"
	"milke/database/repositories"
	"os"

	_ "github.com/joho/godotenv/autoload"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	Users *repositories.UserRepository
)

func Connect() (*gorm.DB, error) {
	db, err := gorm.Open(mysql.Open(os.Getenv("DSN")), &gorm.Config{})

	db.AutoMigrate(&models.User{})

	if err != nil {
		return nil, err
	}

	Users = repositories.NewUserRepository(db)

	return db, nil
}
